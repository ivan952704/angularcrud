import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateEmployeeReactiveComponent } from './create-employee-reactive.component';

// Lazy Loading because you define the prefix in the app-routing
const reactiveEmployeeRoutes: Routes = [
    {
        path: 'create',
        component: CreateEmployeeReactiveComponent
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(reactiveEmployeeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ReactiveEmployeeRoutingModule { }
