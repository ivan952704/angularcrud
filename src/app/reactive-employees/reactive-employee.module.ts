import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ReactiveEmployeeRoutingModule } from './reactive-employee-routing.module';

import { CreateEmployeeReactiveComponent } from './create-employee-reactive.component';

@NgModule({
  declarations: [
    CreateEmployeeReactiveComponent
  ],
  imports: [
    ReactiveEmployeeRoutingModule,
    SharedModule
  ]
})
export class ReactiveEmployeeModule {}
