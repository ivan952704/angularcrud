import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, NgForm, FormArray, FormControl } from '@angular/forms';
import { CustomValidators } from '../shared/custom.validators';

@Component({
    selector: 'app-create-employee-reactive',
    styleUrls: ['./create-employee-reactive.component.css'],
    templateUrl: './create-employee-reactive.component.html'
})
export class CreateEmployeeReactiveComponent implements OnInit {

    employeeForm: FormGroup;
    // @ViewChild('employeeFormTrv') public employeeFormTrv: NgForm;
    submitted = false;
    fullNameLength = 0;

    formErrors = {};

    validationMessages = {
        'fullName': {
            'required': 'Full Name is required.',
            'minlength': 'Full Name must be greater than 2 characters.',
            'maxlength': 'Full Name must be less than 10 characters.'
        },
        'email': {
            'required': 'Email is required.',
            'emailDomain': 'Email domain should be gmail.com'
        },
        'confirmEmail': {
            'required': 'Confirm Email is required.'
        },
        'phone': {
            'required': 'Phone is required.'
        },
        'skillName': {
            'required': 'Skill Name is required.',
        },
        'experienceInYears': {
            'required': 'Experience is required.',
            'max': 'Experience should be less than 11'
        },
        'proficiency': {
            'required': 'Proficiency is required.',
        },
        'emailGroup': {
            'emailMismatch': 'Email and Confirm Email do not match.'
        }
    };

    constructor(private _formBuilder: FormBuilder) { }

    ngOnInit() {
        this.employeeForm = this._formBuilder.group({
            fullName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
            contactPreference: ['email'],
            emailGroup: this._formBuilder.group({
                email: ['', [Validators.required, CustomValidators.emailDomain('gmail.com')]],
                confirmEmail: ['', Validators.required],
            }, { validator: CustomValidators.matchEmail }),
            phone: [''],
            skills: this._formBuilder.array([
                this.addSkillFormGroup()
            ])
        });

        this.employeeForm.get('fullName').valueChanges.subscribe(
            (value: string) => this.fullNameLength = value.length
        );

        this.employeeForm.get('contactPreference').valueChanges.subscribe(
            (value: string) => this.onContactPreferenceChange(value)
        );

        this.employeeForm.valueChanges.subscribe(
            value => {
                // console.log(JSON.stringify(value));
                this.logValidationErrors(this.employeeForm);
                // console.log(this.formErrors);
            }
        );
    }

    addSkillButtonClick() {
        (<FormArray>this.employeeForm.get('skills')).push(this.addSkillFormGroup());
    }

    addSkillFormGroup(): FormGroup {
        return this._formBuilder.group({
            skillName: ['', Validators.required],
            experienceInYears: ['', [Validators.required, Validators.max(10)]],
            proficiency: ['beginner', Validators.required]
        });
    }

    removeSkillButtonClick(index: number) {
        const skillsFormArray = <FormArray>this.employeeForm.get('skills');
        skillsFormArray.removeAt(index);
        skillsFormArray.markAsDirty();
        skillsFormArray.markAsTouched();
    }

    logKeyValuePairs(group: FormGroup) {
        Object.keys(group.controls).forEach((key: string) => {
            const abstractControl = group.get(key);
            if (abstractControl instanceof FormGroup) {
                this.logKeyValuePairs(abstractControl);
                abstractControl.disable();
            } else {
                console.log(`Key = ${key} + Value = ${abstractControl.value}`);
                abstractControl.markAsDirty();
            }
        });
    }

    logValidationErrors(group: FormGroup = this.employeeForm) {

        Object.keys(group.controls).forEach((key: string) => {
            const abstractControl = group.get(key);

            this.formErrors[key] = '';
            if (abstractControl && !abstractControl.valid &&
                (abstractControl.touched || abstractControl.dirty || abstractControl.value !== '' || this.submitted)) {
                const messages = this.validationMessages[key];
                for (const errorKey in abstractControl.errors) {
                    if (errorKey) {
                        this.formErrors[key] += messages[errorKey] + ' ';
                    }
                }
            }

            if (abstractControl instanceof FormGroup) {
                this.logValidationErrors(abstractControl);
            }

            /*
                Now we are using the element item to validate the error

                if (abstractControl instanceof FormArray) {
                for (const control of abstractControl.controls) {
                    if (control instanceof FormGroup) {
                        this.logValidationErrors(control);
                    }
                }
            } */
        });
    }

    onSubmit() {
        this.submitted = true;
        this.logValidationErrors(this.employeeForm);
        if (this.employeeForm.invalid) {
            return;
        }

        console.log('Saved');

        /* console.log(this.employeeForm);
        console.log(this.employeeForm.value);

        console.log(this.employeeForm.controls.fullName.touched);
        console.log(this.employeeForm.get('fullName').touched); */
    }

    onLoadDataClick() {
        // all values, works great when there is no form array child
        /* this.employeeForm.setValue({
            fullName: 'Ivan',
            contactPreference: 'email',
            emailGroup: {
                email: 'icontreras@gmail.com',
                confirmEmail: 'icontreras2@gmail.com',
            },
            phone: ''
        }); */

        // just some fields
        this.employeeForm.patchValue({
            fullName: 'Ivan Contreras',
            contactPreference: 'email',
            emailGroup: {
                email: 'icontreras@gmail.com',
                confirmEmail: 'icontreras2@gmail.com',
            },
            phone: ''
        });

        // Form array
        this.employeeForm.setControl('skills', this.getSkills());
    }

    getSkills(): FormArray {
        const formArray = new FormArray([
            this._formBuilder.group({
                skillName: ['C#', Validators.required],
                experienceInYears: [3, [Validators.required, Validators.max(10)]],
                proficiency: ['intermediate', Validators.required]
            }),
            this._formBuilder.group({
                skillName: ['HTML', Validators.required],
                experienceInYears: [11, [Validators.required, Validators.max(10)]],
                proficiency: ['advanced', Validators.required]
            }),
            this._formBuilder.group({
                skillName: ['PHP', Validators.required],
                experienceInYears: [1, [Validators.required, Validators.max(10)]],
                proficiency: ['beginner', Validators.required]
            })
        ]);

        return formArray;
    }

    onValidateDataClick() {
        // this.logKeyValuePairs(this.employeeForm);
        // this.logValidationErrors(this.employeeForm);
        // console.log(this.formErrors);
    }

    onContactPreferenceChange(selectedValue: string) {
        const phoneControl = this.employeeForm.get('phone');
        if (selectedValue === 'phone') {
            phoneControl.setValidators(Validators.required);
        } else {
            phoneControl.clearValidators();
        }
        phoneControl.updateValueAndValidity();
    }
}
