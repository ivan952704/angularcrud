import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { EmployeeFilterPipe } from './employees/employee-filter.pipe';
import { HomeComponent } from './home.component';
import { PageNotFoundComponent } from './page-not-found.component';

import { EmployeeService } from './employees/employee.service';
import { CreateEmployeeCanDeactivateGuardService } from './employees/create-employee-can-deactivate-guard.service';
import { EmployeeListResolverService } from './employees/employee-list-resolver.service';
import { EmployeeDetailsGuardService } from './employees/employee-details-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeFilterPipe,
    PageNotFoundComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    // Take care of the route orders ** deletes the rest of routes
    // For this example we are using Lazy Loading istead of Eager Loading
    // EmployeeModule,
    AppRoutingModule
  ],
  providers: [
    CreateEmployeeCanDeactivateGuardService,
    EmployeeDetailsGuardService,
    EmployeeListResolverService,
    EmployeeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
