import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageNotFoundComponent } from './page-not-found.component';
import { CreateEmployeeReactiveComponent } from './reactive-employees/create-employee-reactive.component';
import { HomeComponent } from './home.component';

const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  /* { path: 'notfound', component: PageNotFoundComponent }, */

  { path: 'employees', loadChildren: './employees/employee.module#EmployeeModule' },
  { path: 'reactive-employees', loadChildren: './reactive-employees/reactive-employee.module#ReactiveEmployeeModule' },

  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { enableTracing: false })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }

// json-server --watch db.json
