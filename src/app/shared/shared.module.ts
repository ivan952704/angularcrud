import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { AccordionComponent } from './accordion.component';
import { SelectRequiredValidatorDirective } from './select-required-validator.directive';
import { ConfirmEqualValidatorDirective } from './confirm-equal-validator.directive';

@NgModule({
  imports: [
    CommonModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [
    AccordionComponent,
    SelectRequiredValidatorDirective,
    ConfirmEqualValidatorDirective
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    AccordionComponent,
    SelectRequiredValidatorDirective,
    ConfirmEqualValidatorDirective
  ]
})
export class SharedModule { }
