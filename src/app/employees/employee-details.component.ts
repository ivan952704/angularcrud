import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from './employee.service';
import { Employee } from '../models/employee.model';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  employee: Employee;

  private _employeeId: number;
  private _employeesCount: number;

  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _employeeService: EmployeeService) {
      this._employeeService.getEmployees().subscribe(
        (employees) => {
          this._employeesCount = employees.length;
        },
        (error) => console.log(error)
      );
    }

  ngOnInit() {
    // this._employeeId = +this._route.snapshot.paramMap.get('id');
    // this.employee = this._employeeService.getEmployee(this._employeeId);

    // Angular aplica el unsubscribe del metodo

    // El ngOnInit se ejecuta una vez pero estee codigo permite que el servicio de router
    // siempre este escuchando cuando se cambia un parametro de la url actual
    this._route.paramMap.subscribe(params => {
      this._employeeId = +params.get('id');
      this._employeeService.getEmployee(this._employeeId).subscribe(
        (employee) => this.employee = employee,
        (error) => console.log(error)
      );
    });
  }

  viewNextEmployee(): void {
    if (this._employeeId < this._employeesCount) {
      this._employeeId++;
    } else {
      this._employeeId = 1;
    }

    this._router.navigate(['/employees', this._employeeId], {
      queryParamsHandling: 'preserve'
    });
  }

}
