import { NgModule } from '@angular/core';
import { EmployeeRoutingModule } from './employee-routing.module';
import { SharedModule } from '../shared/shared.module';

import { ListEmployeesComponent } from './list-employees.component';
import { CreateEmployeeComponent } from './create-employee.component';
import { DisplayEmployeeComponent } from './display-employee.component';
import { EmployeeDetailsComponent } from './employee-details.component';

@NgModule({
  declarations: [
    ListEmployeesComponent,
    CreateEmployeeComponent,
    DisplayEmployeeComponent,
    EmployeeDetailsComponent
  ],
  imports: [
    EmployeeRoutingModule,
    SharedModule
  ],
  // To use this components in other modules. Now you can use this component in home module for example
  exports: [
    CreateEmployeeComponent
  ]
})
export class EmployeeModule { }
