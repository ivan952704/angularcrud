import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { EmployeeService } from './employee.service';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { Employee } from '../models/employee.model';

@Injectable()
export class EmployeeListResolverService implements Resolve<Employee[] | string> {

    constructor(private _employeeService: EmployeeService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Employee[] | string> {
        // No hacemos un .subscribe porque el resolver automaticamente subscribe el servicio
        // si llamas el servicio en otro componente se tiene que subscribir

        /* return this._employeeService.getEmployees()
            .pipe(
                map((employeeList) => new ResolvedEmployeeList(employeeList)),
                catchError((error: any) => of(new ResolvedEmployeeList(null, error)))
            ); */

        return this._employeeService.getEmployees()
            .pipe(catchError((error: string) => of(error)));
    }
}
