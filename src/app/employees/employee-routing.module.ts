import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListEmployeesComponent } from './list-employees.component';
import { EmployeeDetailsComponent } from './employee-details.component';
import { CreateEmployeeComponent } from './create-employee.component';

import { CreateEmployeeCanDeactivateGuardService } from './create-employee-can-deactivate-guard.service';
import { EmployeeListResolverService } from './employee-list-resolver.service';
import { EmployeeDetailsGuardService } from './employee-details-guard.service';

// Lazy Loading because you define the prefix in the app-routing
const employeeRoutes: Routes = [
  {
    path: '',
    component: ListEmployeesComponent,
    resolve: { employeeList: EmployeeListResolverService }
  },
  {
    path: 'edit/:id',
    component: CreateEmployeeComponent,
    canDeactivate: [CreateEmployeeCanDeactivateGuardService]
  },
  {
    path: ':id',
    component: EmployeeDetailsComponent,
    canActivate: [EmployeeDetailsGuardService]
  }
];

// Eager Loading
/* const employeeRoutes: Routes = [
    {
        path: 'employees',
        children: [
            {
                path: '',
                component: ListEmployeesComponent,
                resolve: { employeeList: EmployeeListResolverService }
            },
            {
                path: 'edit/:id',
                component: CreateEmployeeComponent,
                canDeactivate: [CreateEmployeeCanDeactivateGuardService]
            },
            {
                path: ':id',
                component: EmployeeDetailsComponent,
                canActivate: [EmployeeDetailsGuardService]
            }
        ]
    }
]; */

@NgModule({
  imports: [
    RouterModule.forChild(employeeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class EmployeeRoutingModule { }
