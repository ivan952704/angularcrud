import { Component, OnInit, ViewChild } from '@angular/core';
import { Department } from '../models/department.model';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Employee } from '../models/employee.model';
import { EmployeeService } from './employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  panelTitle: string;

  @ViewChild('employeeForm') public createEmployeeForm: NgForm;

  previewPhoto = false;

  employee: Employee;

  departments: Department[] = [
    { id: 1, name: 'Help Desk' },
    { id: 2, name: 'HR' },
    { id: 3, name: 'IT' },
    { id: 4, name: 'Payroll' }
  ];

  datePickerConfig: Partial<BsDatepickerConfig>;

  constructor(private _employeeService: EmployeeService,
    private _router: Router,
    private _route: ActivatedRoute) {
    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        showWeekNumbers: false,
        minDate: new Date(1990, 0, 1),
        maxDate: new Date(2019, 11, 31)
      }
    );
  }

  ngOnInit() {
    this._route.paramMap.subscribe(params => {
      const id: number = +params.get('id');
      this.getEmployee(id);
    });
  }

  private getEmployee(id: number) {
    if (id === 0) {
      this.employee = {
        id: null,
        name: null,
        gender: 'Male',
        contactPreference: null,
        phoneNumber: null,
        email: null,
        dateOfBirth: new Date(2019, 0, 1),
        department: '-1',
        isActive: true,
        photoPath: null,
        password: null,
        confirmPassword: null
      };
      this.createEmployeeForm.reset();
      this.panelTitle = 'Create Employee';
    } else {
      // this.employee = Object.assign({}, this._employeeService.getEmployee(id));
      this._employeeService.getEmployee(id).subscribe(
        (employee) => {
          employee.dateOfBirth = new Date(employee.dateOfBirth);
          this.employee = employee;
        },
        (error) => console.log(error)
      );
      this.panelTitle = 'Edit Employee';
    }
  }

  togglePhotoPreview(): void {
    this.previewPhoto = !this.previewPhoto;
  }

  saveEmployee(): void {
    // const newEmployee = Object.assign({}, this.employee);

    if (this.createEmployeeForm.invalid) {
      return;
    }

    if (this.employee.id == null) {
      this._employeeService.addEmployee(this.employee).subscribe(
        (data: Employee) => {
          console.log(data);
          this.createEmployeeForm.reset({
            contactPreference: 'phone'
          });
          this._router.navigate(['employees']);
        },
        (error) => console.log(error)
      );
    } else {
      this._employeeService.updateEmployee(this.employee).subscribe(
        () => {
          this.createEmployeeForm.reset({
            contactPreference: 'phone'
          });
          this._router.navigate(['employees']);
        },
        (error) => console.log(error)
      );
    }
  }
}
