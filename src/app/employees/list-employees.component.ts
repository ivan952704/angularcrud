import { Component, OnInit } from '@angular/core';
import { Employee } from '../models/employee.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  // selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css']
})
export class ListEmployeesComponent implements OnInit {

  employees: Employee[];
  filteredEmployees: Employee[];

  dataFromChild: Employee;
  error: string;

  private _searchTerm: string;
  get searchTerm(): string {
    return this._searchTerm;
  }
  set searchTerm(value: string) {
    this._searchTerm = value;
    this.filteredEmployees = this.filterEmployees(value);
  }

  constructor(private _route: ActivatedRoute) {
      const resolvedData: Employee[] | string = this._route.snapshot.data['employeeList'];

      if (Array.isArray(resolvedData)) {
        this.employees = resolvedData;
      } else {
        this.error = resolvedData;
      }

      if (this._route.snapshot.queryParamMap.has('searchTerm')) {
        this.searchTerm = this._route.snapshot.queryParamMap.get('searchTerm');
      } else {
        this.filteredEmployees = this.employees;
      }
    }

  ngOnInit() {
    /*
      En caso se traiga la data directamente sin un Guard Resolver
      this._employeeService.getEmployees().subscribe((list) => {
      this.employees = list;

      if (this._route.snapshot.queryParamMap.has('searchTerm')) {
        this.searchTerm = this._route.snapshot.queryParamMap.get('searchTerm');
      } else {
        this.filteredEmployees = this.employees;
      }
    }); */
  }

  handleNotify(eventData: Employee) {
    this.dataFromChild = eventData;
  }

  filterEmployees(searchString: string): Employee[] {
    return this.employees.filter(x => x.name.toLowerCase().indexOf(searchString.toLowerCase()) !== -1);
  }

  onDeleteNotification(id: number) {
    const foundIndex = this.filteredEmployees.findIndex(x => x.id === id);
    if (foundIndex !== -1) {
      this.filteredEmployees.splice(foundIndex, 1);
    }
  }
}
