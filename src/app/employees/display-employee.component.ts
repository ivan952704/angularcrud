import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Employee } from '../models/employee.model';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeService } from './employee.service';

@Component({
  selector: 'app-display-employee',
  templateUrl: './display-employee.component.html',
  styleUrls: ['./display-employee.component.css']
})
export class DisplayEmployeeComponent implements OnInit, OnChanges {

  @Input() searchTerm: string;
  @Input() employee: Employee;
  @Output() notify: EventEmitter<Employee> = new EventEmitter<Employee>();
  @Output() notifyDelete: EventEmitter<number> = new EventEmitter<number>();
  confirmDelete = false;

  private selectedEmployeeId: number;

  constructor(private _employeeService: EmployeeService,
    private _router: Router,
    private _route: ActivatedRoute) { }

  ngOnInit() {
    this.selectedEmployeeId = +this._route.snapshot.paramMap.get('id');
  }

  ngOnChanges(changes: SimpleChanges) {
    /* if (changes.employee) {
      const previousEmployee = <Employee>changes.employee.previousValue;
      const currentEmployee = <Employee>changes.employee.currentValue;

      console.log('Previous : ' + (previousEmployee ? previousEmployee.name : 'NULL'));
      console.log('Current : ' + currentEmployee.name);
    } */
  }

  handleClick() {
    this.notify.emit(this.employee);
  }

  viewEmployee() {
    this._router.navigate(['/employees', this.employee.id], {
      queryParams: {
        'searchTerm': this.searchTerm
      }
     });
  }

  editEmployee() {
    this._router.navigate(['/employees/edit', this.employee.id]);
  }

  deleteEmployee() {
    this._employeeService.deleteEmployee(this.employee.id).subscribe(
      () => console.log('Deleted'),
      (error) => console.log(error)
    );
    this.notifyDelete.emit(this.employee.id);
  }
}
